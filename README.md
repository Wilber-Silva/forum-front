# README #

O projeto usa React, hooks e material-design.
O desenvolvimento foi feito priorisando agilidade na entrega, então algumas praticas foram ignoradas

## Requisitos ##

* Instalar node v12

## Executar projeto ##

* Baixar dependencias 'npm install' ou 'npm update'
* Executar Projeto 'npm start'

## Em caso de erros de conexão ##

* Verificar se a applicação back-end está sendo executada
* Verificar base url (e atualizar em Integrations caso tenha diferença)
* Vefiricar x-api-key (e atualizar em Integrations caso tenha diferença)

## NightWatch ##

* **Atenção a versão do google crome é muito importante para a execução ser bem sucedida**
* Execute 'npm test'
* (Opicional) é possivel executar os testes de forma unitária com npm run test:tag nomeDaTag
* Caso não aconteça naturalmente crie a pasta src/e2e/tests_outputs

## Features ##

* Criação de perguntas
* Vizualização de perguntas existentes
* Criação de Respostas
* Curtir resposta
* Excluir resposta
* Filtrar por titulo, com respostas ou sem respostas

## Estrutura das pastas ##

* src/ é onde esta toda a applicação
* Components/ são components resutilizaveis
* Integrations/ são as representações da conexão com apis
* Pages/ páginas da aplicação
