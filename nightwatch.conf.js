require('babel-core/register')

const chromerdriver = require('chromedriver')
require('geckodriver') // não tem acesso ao path

const testUrl = 'http://localhost:3000/'
const defaultTimeout = 15000

module.exports = {

  src_folders: ['./src/e2e/tests'],
  // src_folders: ['tests/busca'],

  page_objects_path: './src/e2e/pages',
  globals_path: './src/e2e/hooks/globals.js',

  webdriver: {
    start_process: true
  },

  test_workers:{ enabled: true, workers: 2 }, //só o chrome que funciona mais de uma instancia

  screenshots: {
    enabled: true,
    on_failure: true,
    on_error: true,
    path: './src/e2e/tests_output/'
  },

  test_settings: {
    
    default: {
      launch_url: testUrl,
      globals: {
        waitForConditionTimeout: defaultTimeout
      },
      webdriver: {        
        server_path: chromerdriver.path,
        port: 9515
      },            
      desiredCapabilities: {
        browserName : 'chrome'
      },  
    },

    stage: {
      launch_url: testUrl,
    },

    headless:{
      launch_url: testUrl,
      globals: {
        waitForConditionTimeout: defaultTimeout
      },
      webdriver: {        
        server_path: chromerdriver.path,
        port: 9515
      },            
      desiredCapabilities: {
        browserName : 'chrome',
        chromeOptions: {
          w3c: false,
          args: ['--headless', '--no-sandbox']
        }
      },  
    },

    firefox:{
      launch_url: testUrl,
      globals: {
        waitForConditionTimeout: defaultTimeout
      },
      webdriver: {        
        server_path: '.\\node_modules\\.bin\\geckodriver.cmd',
        port: 4444
      },            
      desiredCapabilities: {
        browserName : 'firefox',
        acceptInsecureCerts: true
      },        
    }
  }
}