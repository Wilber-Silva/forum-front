import React from 'react'
import { Grid } from "@material-ui/core"

import QuestionsContainer from './../Components/Containers/QuestionsContainer'

const Questions = () => (
    <Grid container>
        <QuestionsContainer />
    </Grid>
)

export default Questions