const Actions = { 
    test: function(params) {
      return this
        .navigate()
        .waitForElementVisible('@root', 3000)
        .createQuestion(params)
        .pause(3000)
        .pause(9000)        
    },
    createQuestion: function ({ title, userName, text }) {
        return this
            .waitForElementVisible('@questionUserNameInput', 3000)
            .setValue('@questionUserNameInput', userName)
            .waitForElementVisible('@questionTitleInput', 3000)
            .setValue('@questionTitleInput', title)
            .waitForElementVisible('@questionTextInput', 3000)
            .setValue('@questionUserNameInput', text)
            .waitForElementVisible('@createQuestionButton', 3000)
            .click('@createQuestionButton')
    }    
  }
  
  module.exports = {
    url: '/',
    commands: [ Actions, ],
    elements: {
        root: '#root',
        questionUserNameInput: '#question-userName',
        questionTitleInput: '#question-title',
        questionTextInput: '#question-text',
        createQuestionButton: '#create-question'
    }
  }