import React from 'react'
import { BrowserRouter, Switch, Route } from "react-router-dom"

import QuestionsPage from "./Pages/Questions"

export default () => (
    <BrowserRouter>
        <Switch>
          <Route path="/" component={QuestionsPage}/>
        </Switch>
    </BrowserRouter>
)