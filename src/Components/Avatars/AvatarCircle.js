import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Avatar } from '@material-ui/core'


const useStyles = makeStyles((theme) => (
    {
        border: {
            borderWidth: 2,
            borderStyle: 'solid'
        },
        pointer: {
            cursor: 'pointer'
        },
        small: {
            width: theme.spacing(6),
            height: theme.spacing(6),
        },
        large: {
            width: theme.spacing(10),
            height: theme.spacing(10)
        },
        largeX: {
            width: theme.spacing(20),
            height: theme.spacing(20)
        }
    }
))

export default ({ largeX, large, small, border = false, borderColor, content, pointer, onClick }) => {
    const classes = useStyles()
    let otherClassName = ''

    if (borderColor || border) otherClassName = classes.border
    if (pointer) otherClassName += `${otherClassName} ${classes.pointer}`

    if (small) return <Avatar className={`${classes.small} ${otherClassName}`} style={{borderColor}} onClick={onClick}> {content} </Avatar>
    if (large) return <Avatar className={`${classes.large} ${otherClassName}`} style={{borderColor}}> {content} </Avatar>
    if (largeX) return <Avatar className={`${classes.largeX} ${otherClassName}`} style={{borderColor}}> {content} </Avatar>

    return <Avatar className={`${classes.small} ${otherClassName}`} style={{borderColor}}/>
}