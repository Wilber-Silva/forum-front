import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';
import CardQuestions from './../Cards/CardQuestions'

export default ({ width }) => 
(
    <CardQuestions width={width}
               avatar={<Skeleton animation="wave" variant="circle" width={40} height={40} />} 
               title={<Skeleton animation="wave" height={10} width="80%" style={{ marginBottom: 6 }} />}
               subtitle={<Skeleton animation="wave" height={10} width="50%" style={{ marginBottom: 6 }} />}
               content={<Skeleton variant="rect" height={50} />}
    />
)
