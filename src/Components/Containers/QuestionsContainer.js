import React, { Component } from 'react'
import { Grid, Button, Typography, TextareaAutosize, TextField, CircularProgress, InputAdornment  } from '@material-ui/core'
import { ArrowBack as ArrowBackIcon, Search as SearchIcon } from '@material-ui/icons'

import Load from './../Load/QuestionsLoad'
import CardQuestions from './../Cards/CardQuestions'
import Avatar from './../Avatars/AvatarCircle'
import AnswersPaper from './../Paper/AnswersPaper'

import SelfApiIntegration from './../../Integrations/SelfBackEndIntegration'
import { dateTime } from './../Helpers/FormatDate'

class QuestionsContainer extends Component{
    state = {
        questions: [],
        keepQuestions: null,
        loading: true,
        loadingQuestion: false,
        questionIndex: null,
        text: '',
        title: '',
        userName: ''
    }
    handleChange (field, value) {
        this.setState({
            [field]: value
        })
    }
    handleLikeAnswers (questionIndex, answerIndex) {
        const { questions } = this.state
        const { _id, text, liked } = questions[questionIndex].answers[answerIndex]

        questions[questionIndex].answers[answerIndex].liked = !liked

        SelfApiIntegration.updateAnswers({ 
            _id,
            text,
            liked: !liked
        })
        .then(() => this.setState({questions}))

    }
    handleDeleteAnswers (questionIndex, answerIndex) {
        this.setState({ loadingQuestion: true })

        const { questions } = this.state
        const { _id } = questions[questionIndex].answers[answerIndex]
        questions[questionIndex].answers.splice(answerIndex)

        SelfApiIntegration.deleteAnswers({
            _id
        })
        .then(() => this.setState({ questions, loadingQuestion: false }))
    }
    handleFilter (filterName, value = null) {
        let questions
        switch (filterName) {
            case 'all':
                this.setState({
                    questions: this.state.keepQuestions
                })
                break
            case 'answer':
                questions = this.state.keepQuestions
                this.setState({
                    questions: questions.filter(question => question.answers.length ),
                })
                break
            case 'nofen-answer':
                questions = this.state.keepQuestions
                this.setState({
                    questions: questions.filter(question => question.answers.length === 0 ),
                })
                break
            case 'search':
                questions = this.state.keepQuestions
                this.setState({
                    questions: questions.filter(question => {
                        console.log(String(question.title || '').search(value), String(question.title))
                        return String(question.title || '').search(value) >= 0
                    } ),
                })
                break
            default:
                this.setState({
                    questions: this.state.keepQuestions
                })
                break
        }
    }
    componentDidMount = async () => {
        const questions = await SelfApiIntegration.getQuestions().catch(() => [])
        this.setState({
            questions,
            keepQuestions: questions,
            loading: false
        })
    }
    createQuestion () {
        this.setState({ loading: true })
        SelfApiIntegration.createQuestion({
            userName: this.state.userName,
            text: this.state.text,
            title: this.state.title
        })
        .then(question => {
            const { questions } = this.state
            questions.push(question)
            this.setState({ question, loading: false })
        })
    }
    createAnswers (id, index) {
        this.setState({
            loadingQuestion: true
        })
        SelfApiIntegration.createAnswers({
            questionId: id,
            text: this.state.text,
            userName: this.state.userName
        })
        .then(answer => {
            const { questions } = this.state
            questions[index].answers.push(answer)
            this.setState({ questions, text: '', loadingQuestion: false })
        })
        .catch(() => this.setState({loadingQuestion: false}))
    }
    loadRender () {
        const loop = [1,2,3,4,5]
        return loop.map(i => <Grid item key={i}><Load/></Grid>)
    }
    loadQuestionRender () {
        return (
            <div style={{position: 'relative', width:'100%', height: '100%'}}>
                <CircularProgress color="secondary" style={{animationDuration:'550ms'}}/>
            </div>
        )
    }
    oneQuestionRender () {
        const question = this.state.questions[this.state.questionIndex]
        return (
            <Grid item>
                <CardQuestions  loading={this.state.loadingQuestion}
                                width="98%"
                                avatar={<Avatar small pointer content={<ArrowBackIcon />} onClick={() => this.setState({questionIndex: null})}/>}
                                title={
                                    <Typography variant="subtitle1">
                                        {question.title}
                                    </Typography>
                                }
                                subtitle={
                                    <Typography variant="subtitle2">
                                        {dateTime(question.createdAt)}
                                    </Typography>
                                }
                                content={
                                    <Grid item container direction="column" spacing={2}>
                                            <Grid item> 
                                                <Typography variant="h6">
                                                    {question.text}
                                                </Typography>
                                            </Grid>
                                            <Grid item> 
                                                <Typography variant="inherit">
                                                    Respostas
                                                </Typography>                                                
                                            </Grid>
                                            {
                                                question.answers.map((answers, answersIndex) => (
                                                    <Grid item key={answersIndex}>
                                                        <AnswersPaper 
                                                            liked={answers.liked}
                                                            title={answers.userName}
                                                            postedAt={dateTime(question.createdAt)}
                                                            text={answers.text}
                                                            onLike={() => this.handleLikeAnswers(this.state.questionIndex, answersIndex)}
                                                            onDelete={() => this.handleDeleteAnswers(this.state.questionIndex, answersIndex)}
                                                        />
                                                    </Grid>
                                                ))
                                            }
                                            <Grid item container direction="column" spacing={2}>
                                                <Grid item> 
                                                    <TextField 
                                                            id="answers-userName"
                                                            type="text"
                                                            value={this.state.userName}
                                                            onChange={(e) => this.handleChange('userName', e.target.value)}
                                                            label="Nome de usuário"
                                                            variant="outlined"
                                                            style={{marginBottom: '10px'}}
                                                    />
                                                    <TextareaAutosize 
                                                                    id="answers-text"
                                                                    rowsMax={20}
                                                                    rowsMin={10}
                                                                    style={{width:'100%'}}
                                                                    value={this.state.text}
                                                                    onChange={(e) => this.handleChange('text', e.target.value)}
                                                    />
                                                </Grid>
                                                <Grid item>
                                                    <Button variant="contained" color="primary" onClick={() => this.createAnswers(question._id, this.state.questionIndex)}>
                                                        Publique sua respostas
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                    </Grid>
                                }
                    />
            </Grid>
        )
    }
    questionsRender () {
        if (this.state.loading) return this.loadRender()

        return (
            [
                <Grid item>
                    <Grid item container direction="row">
                        <Grid item>
                            <Button variant="contained" color="primary" style={{margin: 5}} onClick={() => this.handleFilter('all')}>
                                Todas
                            </Button>
                        </Grid>
                        <Grid item>
                            <Button variant="contained" color="secondary" style={{margin: 5}} onClick={() => this.handleFilter('answer')}>
                                Respondidas
                            </Button>
                        </Grid>
                        <Grid item >
                            <Button variant="contained" color="inherit" style={{margin: 5}} onClick={() => this.handleFilter('nofen-answer')}>
                                Sem resposta
                            </Button>
                        </Grid>
                        <Grid item>
                            <TextField
                                id="search"
                                onChange={(e) => this.handleFilter('search', e.target.value)}
                                label="Pesquisar por tirulo"
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <SearchIcon />
                                        </InputAdornment>
                                    )
                                }}
                            />
                        </Grid>
                    </Grid>
                </Grid>,
                this.state.questions.map((question, index) => 
                <Grid item key={index}>
                    <Button style={{width: '100%'}} onClick={() => this.setState({ questionIndex: index })}>
                        <CardQuestions
                                    width="98%"
                                    avatar={<Avatar small content="?"/>}
                                    title={
                                        <Typography variant="subtitle1">
                                            {question.title}
                                        </Typography>
                                    }
                                    subtitle={
                                        <Typography variant="subtitle2">
                                             {dateTime(question.createdAt)}
                                        </Typography>
                                    }
                                    content={
                                        <Grid container direction="row-reverse" spacing={2}>
                                            <Grid item xs={2}>
                                                {question.answers.length}
                                            </Grid>
                                            <Grid item xs={10}>
                                                <Typography variant="inherit">
                                                    {question.text}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    }
                        />
                    </Button>
                </Grid>
            ),
            <Grid item>
                <Grid item container direction="column" spacing={5} style={{padding: 10}}>
                    <Grid item>
                        <TextField 
                                id="question-userName"
                                type="text"
                                fullWidth
                                value={this.state.userName}
                                onChange={(e) => this.handleChange('userName', e.target.value)}
                                label="Nome de usuário"
                                variant="outlined"
                        />
                    </Grid>
                    <Grid item>
                        <TextField 
                                id="question-title"
                                type="text"
                                fullWidth
                                value={this.state.title}
                                onChange={(e) => this.handleChange('title', e.target.value)}
                                label="Título da pergunta"
                                variant="outlined"
                        />
                    </Grid>
                    <Grid item>
                        <TextareaAutosize 
                                        id="question-text"
                                        rowsMax={20}
                                        rowsMin={10}
                                        style={{width:'50%'}}
                                        value={this.state.text}
                                        onChange={(e) => this.handleChange('text', e.target.value)}
                        />                              
                    </Grid>
                    <Grid item>
                        <Button id="create-question" variant="contained" color="primary" onClick={() => this.createQuestion()}>
                            Publique sua pergunta
                        </Button>      
                    </Grid>
                </Grid>    
            </Grid>

            ]
        )

    }
    render () {
        return (
            <Grid item container direction="column" spacing={2}>
                {
                    this.state.questionIndex || this.state.questionIndex === 0 ?
                        this.oneQuestionRender()
                        :
                        this.questionsRender()
                }
            </Grid>
        )
    }
}

export default QuestionsContainer