import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography, Paper, IconButton, Checkbox } from '@material-ui/core'
import { People as AvatarIcon, Favorite as FavoriteIcon, FavoriteBorder as FavoriteBorderIcon, Delete as TrashIcon,  } from '@material-ui/icons'

import Avatar from './../Avatars/AvatarCircle'

const useStyles = makeStyles((theme) => (
    {
        paper: {
            padding: 10,
            margin: 0
        }
    }
))

const AnswersPaper = ({ title, text, liked, postedAt, onLike, onDelete }) => {
    const classes = useStyles()
    return (
        <Paper className={classes.paper}>
            <Grid container direction="row">
                <Grid item xs={1}>
                    <Avatar content={<AvatarIcon />}/>
                </Grid>
                <Grid item xs={11}>
                    <Typography variant="subtitle1">
                        {title} <small>{postedAt}</small>
                    </Typography>
                    <Typography variant="inherit">
                        {text}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Checkbox 
                        checked={liked}
                        icon={<FavoriteBorderIcon />}
                        checkedIcon={<FavoriteIcon />}
                        onChange={onLike}
                    />

                    <IconButton onClick={onDelete}>
                        <TrashIcon />
                    </IconButton>
                </Grid>
            </Grid>
        </Paper>
    )
}

export default AnswersPaper