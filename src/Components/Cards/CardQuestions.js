import React from 'react';
import { Card, CardHeader, CardContent, CircularProgress } from '@material-ui/core'

const LoadCircular = () => (
    <div style={{
            position: 'absolute',
            width:'100%',
            height: '100%',
            backgroundColor: 'rgba(255,255,255, 0.5)',
            cursor: 'wait',
            paddingLeft: '50%',
            paddingTop: '20%'
        }}>
        <CircularProgress color="primary" style={{animationDuration:'550ms'}}/>
    </div>
)

export default ({ width, avatar, title, content, subtitle, className, loading }) => 
(
    <Card style={{ width, padding: 10, margin: 10, textAlign:'left', overflow: 'hidden' }} className={className}>
        <CardHeader avatar={avatar} 
                    title={title}
                    subheader={subtitle}
        />
        
        <CardContent>
            { loading && <LoadCircular /> }
            {content}
        </CardContent>
    </Card>
)
