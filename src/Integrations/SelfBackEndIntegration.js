import axios from 'axios'

class SelfBackEndIntegration {
    http = null
    constructor() {
        this.http = axios.create({
            baseURL: 'http://localhost:3001',
            headers: {
                'x-api-key': 'd41d8cd98f00b204e9800998ecf8427e'
            }
        })
    }
    getQuestions = async () => {
        const { data } = await this.http.get('/question')
        return data
    }
    createQuestion = async ({ userName, text, title }) => {
        const { data } = await this.http.post('/question', { userName, text, title })
        return data
    }
    
    createAnswers = async ({ questionId, text, userName }) => {
        const { data } = await this.http.post('/answers', { questionId, text, userName })
        return data
    }
    updateAnswers = async ({ _id, liked, text }) => {
        await this.http.put(`/answers/${_id}`, { liked, text })
    }
    deleteAnswers = async ({ _id }) => {
        await this.http.delete(`/answers/${_id}`)
    }
}


export default new SelfBackEndIntegration()